# Copyright (c) 2020, 2023, Oracle Corporation and/or its affiliates.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

introductionHTML: >
  <p>This page displays the list of Java EE applications and standalone application modules installed to this domain.</p>
  <p>Use it to start and stop an application, view the application's intended and current state, and to manage the application's deployment plan.</p>
  <p>Note: Use 'Application Runtimes', instead of this page, to view the applications' runtime information.</p>

displayedColumns:
- name: "Name"
- name: "State.IntendedState"
- name: "ApplicationName"
- name: "ApplicationVersion"
- name: "DeploymentPlan.PlanPath"

actions:
- name: "startActions"
  label: "Start"
  actions:
  - name: "start"
    label: "Servicing all requests"
  - name: "startInAdminMode"
    label: "Servicing only administration requests"
- name: "stopActions"
  label: "Stop"
  actions:
  - name: "stop"
    label: "When work completes"
  - name: "forceStop"
    label: "Force stop now"
  - name: "stopToAdminMode"
    label: "Servicing non-administration requests"
- name: "redeploy"
- name: "update"
- name: "createPlan"
  rows: "one"

helpTopics:
- label: "Developing Applications for Oracle WebLogic Server"
  href: "wlprg/index.html"
  type: "edocs"
- label: "Deploying Applications to Oracle WebLogic Server"
  href: "depgd/index.html"
  type: "edocs"
