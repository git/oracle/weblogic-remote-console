# Copyright (c) 2021, 2023, Oracle Corporation and/or its affiliates.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.
actions:

- name: "start"
  label: "Start servicing all requests"

- name: "startInAdminMode"
  label: "Start servicing only administration requests"
  actionMethod: weblogic.remoteconsole.customizers.AppDeploymentRuntimeMBeanCustomizer.startInAdminMode
  mbeanOperation:
    operation: start_targets_deploymentOptions
  helpHTML: >
    <p>Start this application in admin mode on all running servers to which it is deployed.
    This is an asynchronous operation that returns immediately.
    The returned DeploymentProgressObjectMBean can be used to determine when the operation is completed.</p>
  # don't specify polling since the current page only shows the intended state, not the current state
  definition:
    type: "weblogic.management.runtime.DeploymentProgressObjectMBean"

- name: "stop"
  label: "Stop when work completes"
  # don't specify polling since the current page only shows the intended state, not the current state

- name: "forceStop"
  label: "Force stop now"
  actionMethod: weblogic.remoteconsole.customizers.AppDeploymentRuntimeMBeanCustomizer.forceStop
  mbeanOperation:
    operation: stop_targets_deploymentOptions
  # don't specify polling since the current page only shows the intended state, not the current state
  helpHTML: >
    <p>Forcibly stop this application on all running servers to which it is deployed.
    This is an asynchronous operation that returns immediately.
    The returned DeploymentProgressObjectMBean can be used to determine when the operation is completed.</p>
  definition:
    type: "weblogic.management.runtime.DeploymentProgressObjectMBean"

- name: "stopToAdminMode"
  label: "Stop servicing non-administration requests"
  actionMethod: weblogic.remoteconsole.customizers.AppDeploymentRuntimeMBeanCustomizer.stopToAdminMode
  mbeanOperation:
    operation: stop_targets_deploymentOptions
  # don't specify polling since the current page only shows the intended state, not the current state
  helpHTML: >
    <p>Gracefully stop this application to admin mode on all running servers to which it is deployed.
    This is an asynchronous operation that returns immediately.
    The returned DeploymentProgressObjectMBean can be used to determine when the operation is completed.</p>
  definition:
    type: "weblogic.management.runtime.DeploymentProgressObjectMBean"

- name: "createPlan"
  label: "Create Plan"
  disableMBeanJavadoc: true
  polling:
    reloadSeconds: 5
    maxAttempts: 10
  # temporary workaround for the CFE using the action's pageSummaryHTML as the
  # action input form's introductionHTML
  helpHTML: >
    <p>The deployment plan file name must have a <code>.xml</code> file extension, and should be located
    in its own directory to avoid it being inadvertently shared with other applications.</p>
    <p><code>Plan.xml</code> is the highly recommended name for the file.</p>
    <p><i>The plan file will be overwritten if it already exists.  Other files located in its directory may be overwritten too.</i></p>
  inputForm:
    customizePageMethod: weblogic.remoteconsole.customizers.AppDeploymentRuntimeMBeanCustomizer.customizeCreatePlanActionInputForm
    introductionHTML: >
      <p>The deployment plan file name must have a <code>.xml</code> file extension, and should be located
      in its own directory to avoid it being inadvertently shared with other applications.</p>
      <p><code>Plan.xml</code> is the highly recommended name for the file.</p>
      <p><i>The plan file will be overwritten if it already exists.  Other files located in its directory may be overwritten too.</i></p>
    parameters:
    - name: "PlanPath"

- name: "redeploy"
  label: "Redeploy"
  actionMethod: weblogic.remoteconsole.customizers.AppDeploymentRuntimeMBeanCustomizer.redeploy
  mbeanOperation:
    operation: redeploy_targets_applicationPath_plan_deploymentOptions
  # don't specify polling since nothing on the current page changes when the redeploy finishes
  helpHTML: >
    <p>Redeploy the application in the background for the targets specified
    with the options specified.  This is an asynchronous operation that
    returns immediately.  The returned DeploymentProgressObjectMBean
    can be used to determine when the operation is completed.</p>
  definition:
    type: "weblogic.management.runtime.DeploymentProgressObjectMBean"
    roles:
      allowed:
      - Deployer

- name: "redeploy_targets_applicationPath_plan_deploymentOptions"
  disableMBeanJavadoc: true

- name: "update"
  label: "Update"
  actionMethod: weblogic.remoteconsole.customizers.AppDeploymentRuntimeMBeanCustomizer.update
  mbeanOperation:
    operation: update_targets_plan_deploymentOptions
  # don't specify polling since nothing on the current page changes when the update finishes
  helpHTML: >
     <p>Allows for updating an application configuration using an changed
     deployment plan.</p>
     <p>A configuration update is equivalent to replacing the application's
     deployment plan. The deployment plan is redeployed in-place. A new
     version of the application is not started, even if the new deployment
     plan has a different version.</p>
     <p>The update succeeds only if changes to the deployment plan do not
     require the application to be restarted. Configuration changes in 
     the new deployment plan must be limited to
     tuning the application. Changes to resource bindings causes
     the update to fail. Use redeploy()
     to apply resource binding changes to a production application.</p>
     <p>This method targets only root modules. Module level targeting is 
     not supported.</p>
  definition:
    type: "weblogic.management.runtime.DeploymentProgressObjectMBean"
    roles:
      allowed:
      - Deployer

- name: "update_targets_plan_deploymentOptions"
  disableMBeanJavadoc: true

children:
- name: DeploymentPlan
  requiredCapabilities:
  - DeploymentPlans
- name: State
  requiredCapabilities:
  - DeploymentState
